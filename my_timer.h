#ifndef MY_TIMER_H
#define MY_TIMER_H

#ifdef TIMING

#include <stdio.h>
#include <sys/time.h>

enum {
  BWA_IDX_LOAD,
  BWT_RESTORE_BWT,
  BWT_RESTORE_SA,
  BNS_RESTORE,
  READ_PAC,
  KT_PIPELINE,
  BSEQ_READ,
  MEM_PROCESS_SEQS,
  KT_FOR_1,
  KT_FOR_2,
  OUTPUT,
  MY_TIMER_NUM_TIMERS
};


static double my_timer_time[MY_TIMER_NUM_TIMERS];
static double my_timer_time0[MY_TIMER_NUM_TIMERS];

static inline double my_timer_get_current_time()
{
  struct timeval tv;
  gettimeofday(&tv, 0);
  return (double)tv.tv_sec + (double)tv.tv_usec * 1.0e-6;
}

static inline void my_timer_start(int i)
{
  my_timer_time0[i] = my_timer_get_current_time();
}

static inline void my_timer_stop(int i)
{
  my_timer_time[i] += my_timer_get_current_time() - my_timer_time0[i];
}


#define MY_TIMER_START(i)	my_timer_start(i)
#define MY_TIMER_STOP(i)	my_timer_stop(i)

#define MY_TIMER_PRINT(i) \
  fprintf(stderr, ">>> %s: %g [sec]\n", #i, my_timer_time[i])

#define MY_TIMER_STOP_PRINT(i) \
  fprintf(stderr, ">>> %s: %g [sec]\n", \
    #i, my_timer_get_current_time() - my_timer_time0[i])

#else

#define MY_TIMER_START(i)	((void)0)
#define MY_TIMER_STOP(i)	((void)0)
#define MY_TIMER_PRINT(i)	((void)0)
#define MY_TIMER_STOP_PRINT(i)	((void)0)

#endif

#endif /* MY_TIMER_H */
